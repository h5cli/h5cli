[![Code style: black](https://img.shields.io/badge/code%20style-black-000000.svg)](https://github.com/python/black)

## h5cli
A command shell interface to interact with HDF5 files.

### Usage:

```
h5cli path/to/file.h5
```
